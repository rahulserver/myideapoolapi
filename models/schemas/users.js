const Sequelize = require('sequelize');

const userSchema = {
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true,
    },
    allowNull: false,
    unique: true,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
  },
  avatar_url: {
    type: Sequelize.STRING,
    validate: {
      isUrl: true,
    },
    allowNull: false,
  },
};

module.exports = userSchema;
