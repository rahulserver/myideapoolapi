const Sequelize = require('sequelize');

const ideaSchema = {
  content: {
    type: Sequelize.STRING,
    validate: {
      len: [1, 255],
    },
    allowNull: false,
  },
  impact: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: 1,
      max: 10,
    },
  },
  ease: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: 1,
      max: 10,
    },
  },
  confidence: {
    type: Sequelize.INTEGER,
    allowNull: false,
    validate: {
      min: 1,
      max: 10,
    },
  },
  average_score: {
    type: Sequelize.FLOAT,
    allowNull: false,
  },
};

module.exports = ideaSchema;
