const Sequelize = require('sequelize');
const { sequelize } = require('../initsequelize');
const ideaSchema = require('./schemas/ideas');

class Idea extends Sequelize.Model {}

Idea.init(ideaSchema, {
  sequelize,
  modelName: 'idea',
  createdAt: 'created_at',
  updatedAt: 'updated_at',
  hooks: {
    beforeValidate: async (idea) => {
      // eslint-disable-next-line no-param-reassign
      idea.average_score = (idea.confidence + idea.impact + idea.ease) / parseFloat(3);
    },
    beforeUpdate: async (idea) => {
      // eslint-disable-next-line no-param-reassign
      idea.average_score = (idea.confidence + idea.impact + idea.ease) / parseFloat(3);
    },
  },
});

module.exports = Idea;
