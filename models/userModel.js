const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const gravatar = require('gravatar');
const { sequelize } = require('../initsequelize');
const userSchema = require('./schemas/users');

class User extends Sequelize.Model {}

User.init(userSchema, {
  sequelize,
  modelName: 'user',
  hooks: {
    beforeValidate: async (user) => {
      const salt = await bcrypt.genSalt(parseInt(process.env.ROUNDS, 10));
      // eslint-disable-next-line no-param-reassign
      user.password = await bcrypt.hash(user.password, salt);

      // generate the gravatar
      // eslint-disable-next-line no-param-reassign
      user.avatar_url = gravatar.url(user.email, { protocol: 'https', s: 200 });
    },
  },
});

module.exports = User;
