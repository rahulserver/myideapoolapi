const { describe, it } = require('mocha');
const request = require('supertest');
let app = require('../../app');

if (process.env.ENVIRONMENT === 'testProd') {
  app = process.env.PROD_URL;
}

describe('POST /users', () => {
  // eslint-disable-next-line func-names
  it('should return 201 for valid user', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users')
      .send({
        email: 'sometestuser@gmail.com',
        name: 'test',
        password: 'test',
      })
      .expect(201, done);
  });

  // eslint-disable-next-line func-names
  it('should contain jwt and refresh_token for valid user created', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users')
      .send({
        email: 'someuser2@gmail.com',
        name: 'rahul',
        password: 'admin',
      })
      .expect(201)
      .end((err, res) => {
        res.body.should.have.property('jwt');
        res.body.should.have.property('refresh_token');
        done();
      });
  });

  // eslint-disable-next-line func-names
  it('should return 400 for invalid user', function (done) {
    this.timeout(10000);
    request(app)
      .post('/users')
      .send({
        email: 'rahulserver gmail.com',
        password: 'admin',
      })
      .expect(400, done);
  });
});
