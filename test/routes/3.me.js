const { describe, it } = require('mocha');
const request = require('supertest');
let app = require('../../app');

if (process.env.ENVIRONMENT === 'testProd') {
  app = process.env.PROD_URL;
}

describe('GET /me', () => {
  // eslint-disable-next-line func-names
  it('should return status 200 for valid jwt header', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .get('/me')
          .set('X-Access-Token', res.body.jwt)
          .expect(200, done);
      });
  });

  // eslint-disable-next-line func-names
  it('should contain email, name and avatar_url', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .get('/me')
          .set('X-Access-Token', res.body.jwt)
          .expect(200)
          .end((err2, res2) => {
            res2.body.should.have.property('email');
            res2.body.should.have.property('name');
            res2.body.should.have.property('avatar_url');
            done();
          });
      });
  });
});
