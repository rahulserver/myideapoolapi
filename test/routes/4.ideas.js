const { describe, it } = require('mocha');
const request = require('supertest');
const logger = require('../../logger');
let app = require('../../app');

if (process.env.ENVIRONMENT === 'testProd') {
  app = process.env.PROD_URL;
}

describe('POST /ideas', () => {
  // eslint-disable-next-line func-names
  it('should return 201 for valid idea(after auth)', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'someuser2@gmail.com',
        password: 'admin',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/ideas')
          .set('X-Access-Token', res.body.jwt)
          .send({
            content: 'the content',
            impact: 8,
            ease: 8,
            confidence: 8,
          })
          .expect(201, done);
      });
  });

  // eslint-disable-next-line func-names
  it('response should contain id, content, impact, ease, confidence, average_score and created_at for valid idea(after auth)', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'someuser2@gmail.com',
        password: 'admin',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/ideas')
          .set('X-Access-Token', res.body.jwt)
          .send({
            content: 'the content',
            impact: 8,
            ease: 8,
            confidence: 8,
          })
          .expect(201)
          .end((err2, res2) => {
            logger(res2.body);
            res2.body.should.have.property('id');
            res2.body.should.have.property('content');
            res2.body.should.have.property('impact');
            res2.body.should.have.property('ease');
            res2.body.should.have.property('confidence');
            res2.body.should.have.property('average_score');
            res2.body.should.have.property('created_at');
            done();
          });
      });
  });
});

describe('DELETE /ideas/:id', () => {
  // eslint-disable-next-line func-names
  it('should return 204 for valid idea deleted (after auth)', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'someuser2@gmail.com',
        password: 'admin',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/ideas')
          .set('X-Access-Token', res.body.jwt)
          .send({
            content: 'the content to delete',
            impact: 8,
            ease: 8,
            confidence: 8,
          })
          .expect(201)
          .end((err2, res2) => {
            request(app)
              .delete(`/ideas/${res2.body.id}`)
              .set('X-Access-Token', res.body.jwt)
              .expect(204)
              .end(() => {
                done();
              });
          });
      });
  });
});

describe('PUT /ideas/:id', () => {
  // eslint-disable-next-line func-names
  it('should return 200 for valid idea updated and response body must contain updated data (after auth)', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'someuser2@gmail.com',
        password: 'admin',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/ideas')
          .set('X-Access-Token', res.body.jwt)
          .send({
            content: 'the content to update',
            impact: 8,
            ease: 8,
            confidence: 8,
          })
          .expect(201)
          .end((err2, res2) => {
            request(app)
              .put(`/ideas/${res2.body.id}`)
              .set('X-Access-Token', res.body.jwt)
              .send({
                content: 'updated content',
              })
              .expect(200)
              .end((err3, res3) => {
                logger(`res3body ${JSON.stringify(res3.body)}`);
                res3.body.should.have.property('id');
                res3.body.should.have.property('content');
                res3.body.should.have.property('impact');
                res3.body.should.have.property('ease');
                res3.body.should.have.property('confidence');
                res3.body.should.have.property('average_score');
                res3.body.should.not.have.property('updatedAt');
                res3.body.should.not.have.property('createdAt');
                res3.body.should.have.property('updated_at');
                res3.body.should.have.property('created_at');
                done();
              });
          });
      });
  });
});
