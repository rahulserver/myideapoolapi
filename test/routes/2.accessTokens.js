const { describe, it } = require('mocha');
const request = require('supertest');
let app = require('../../app');

if (process.env.ENVIRONMENT === 'testProd') {
  app = process.env.PROD_URL;
}

describe('POST /access-tokens', () => {
  // eslint-disable-next-line func-names
  it('should return 201 for valid credentials', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201, done);
  });

  // eslint-disable-next-line func-names
  it('should contain jwt and refresh_token for valid user credentials', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        res.body.should.have.property('jwt');
        res.body.should.have.property('refresh_token');
        done();
      });
  });
});

describe('DELETE /access-tokens', () => {
  // eslint-disable-next-line func-names
  it('should return 204', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .delete('/access-tokens')
          .set('X-Access-Token', res.body.jwt)
          .send({
            refresh_token: res.body.refresh_token,
          })
          .expect(204, done);
      });
  });

  // eslint-disable-next-line func-names
  // it('should return 401 after token expiration(5 sec)', function (done) {
  //   this.timeout(10000);
  //   request(app)
  //     .post('/access-tokens')
  //     .send({
  //       email: 'sometestuser@gmail.com',
  //       password: 'test',
  //     })
  //     .expect(201)
  //     .end((err, res) => {
  //       setTimeout(() => {
  //         request(app)
  //           .delete('/access-tokens')
  //           .set('X-Access-Token', res.body.jwt)
  //           .send({
  //             refresh_token: res.body.refresh_token,
  //           })
  //           .expect(401, done);
  //       }, 5001);
  //     });
  // });
});

describe('POST /access-tokens/refresh', () => {
  // eslint-disable-next-line func-names
  it('should return status 200 for valid refresh token', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/access-tokens/refresh')
          .send({
            refresh_token: res.body.refresh_token,
          })
          .expect(200, done);
      });
  });

  // eslint-disable-next-line func-names
  it('should return valid jwt for valid refresh token', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens')
      .send({
        email: 'sometestuser@gmail.com',
        password: 'test',
      })
      .expect(201)
      .end((err, res) => {
        request(app)
          .post('/access-tokens/refresh')
          .send({
            refresh_token: res.body.refresh_token,
          })
          .expect(200)
          .end((err2, res2) => {
            res2.body.should.have.property('jwt');
            done();
          });
      });
  });

  // eslint-disable-next-line func-names
  it('should return status 403 for invalid refresh token', function (done) {
    this.timeout(10000);
    request(app)
      .post('/access-tokens/refresh')
      .send({
        refresh_token: 'yadayada',
      })
      .expect(403, done);
  });
});
