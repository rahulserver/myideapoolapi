const prepare = require('mocha-prepare');

// eslint-disable-next-line global-require
require('dotenv').config();
require('chai').should();

if (process.env.ENVIRONMENT !== 'testProd') {
  // eslint-disable-next-line global-require
  const initsequelize = require('../initsequelize');

  prepare(
    async (done) => {
      // called before loading of test cases
      // initialize sequelize so that it can be used in routes
      await initsequelize.init();
      done();
    },
    async (done) => {
      // called after all test completes (regardless of errors)
      done();
    },
  );
}
