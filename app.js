const express = require('express');
const bodyParser = require('body-parser');
const tokenUtils = require('./utils/tokenUtils');
const handleError = require('./handleError');
const User = require('./models/userModel');
const logger = require('./logger');

const app = express();
const UNAUTHENTICATED_ROUTES = [
  {
    method: 'POST',
    path: '/users',
  },
  {
    method: 'GET',
    path: '/',
  },
  {
    method: 'POST',
    path: '/access-tokens',
  },
  {
    method: 'POST',
    path: '/access-tokens/refresh',
  },
];
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: 'true' })); // parse application/x-www-form-urlencoded

const usersRoute = require('./routes/users');
const tokensRoute = require('./routes/accessTokens');
const meRoute = require('./routes/me');
const ideasRoute = require('./routes/ideas');

const auth = async (req, res, next) => {
  try {
    // check for unauthenticated routes
    const matches = UNAUTHENTICATED_ROUTES.filter(
      item => item.method === req.method && item.path === req.url,
    );

    if (matches.length > 0) {
      return next();
    }

    let token = req.header('X-Access-Token');

    if (!token) {
      throw new Error();
    }

    token = token.trim();
    const decoded = tokenUtils.verifyJWT(token);
    logger(`decoded: ${JSON.stringify(decoded)}`);
    const user = await User.findOne({ where: { email: decoded.email } });

    if (!user) {
      throw new Error();
    }
    req.token = token;
    req.user = user;
    return next();
  } catch (error) {
    logger(error);
    return handleError(res, new Error('Not Authenticated'), 401);
  }
};

app.use(auth);

// root route for testing that end points work
app.get('/', (req, res) => res.send('It Works!'));
app.use('/users', usersRoute);
app.use('/access-tokens', tokensRoute);
app.use('/me', meRoute);
app.use('/ideas', ideasRoute);
module.exports = app;
