// initialize sequelize
const Sequelize = require('sequelize');
const pg = require('pg');

pg.defaults.ssl = true;

let CONNECTION_STRING = null;
if (process.env.DATABASE_URL) {
  CONNECTION_STRING = process.env.DATABASE_URL;
} else {
  throw new Error('process.env.DATABASE_URL missing');
}

const sequelize = new Sequelize(CONNECTION_STRING);
const logger = require('./logger');

const init = () => sequelize.authenticate().then(async () => {
  logger('Connection successful');
  // eslint-disable-next-line global-require
  require('./models/models.bootstrap');
  // sync all tables. force: true for resetting everything. Not to be used in production
  await sequelize.sync({ force: !(process.env.ENVIRONMENT === 'prod') });
  return null;
});

module.exports = {
  sequelize,
  init,
};
