if (process.env.ENVIRONMENT !== 'prod') {
  // eslint-disable-next-line global-require
  require('dotenv').config();
}

const PORT = process.env.PORT || 3000;

// initialize sequelize
const sequelizeInitializer = require('./initsequelize');
const logger = require('./logger');

const app = require('./app');

sequelizeInitializer
  .init()
  .then(() => {
    app.listen(PORT, (err) => {
      if (err) throw err;
      logger('app listening on port 3000');
      logger(`DATABASE_URL=${process.env.DATABASE_URL}`);
    });

    return null;
  })
  .catch(err => logger('initialization failed due to: ', err));
