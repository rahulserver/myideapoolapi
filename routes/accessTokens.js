const express = require('express');
const bcrypt = require('bcrypt');

const router = express.Router();
const User = require('../models/userModel');
const handleError = require('../handleError');
const tokenUtils = require('../utils/tokenUtils');
const storeUtils = require('../utils/storeUtils');
const logger = require('../logger');

router.post('/', async (req, res) => {
  const { email, password } = req.body;
  const userRecord = await User.findOne({ where: { email } });
  if (!userRecord.dataValues) {
    return handleError(res, new Error('No such user exists'), 401);
  }

  const user = userRecord.dataValues;
  const isValid = await bcrypt.compare(password, user.password);
  logger(`user=${JSON.stringify(user)}`);
  if (isValid) {
    const refreshToken = await tokenUtils.generateRefreshTokenPromise();
    const jwt = tokenUtils.createJWT({ email: user.email });
    try {
      await storeUtils.set(refreshToken, jwt);
    } catch (e) {
      logger(e);
      return handleError(res, new Error('Unexpected Error'), 500);
    }
    return res.status(201).send({
      jwt,
      refresh_token: refreshToken,
    });
  }

  return handleError(res, new Error('Invalid credentials!'), 401);
});

router.delete('/', async (req, res) => {
  try {
    // eslint-disable-next-line camelcase
    const { refresh_token } = req.body;
    storeUtils.remove(refresh_token);
    return res.status(204).send();
  } catch (e) {
    return handleError(res, new Error('Unexpected error occurred'), 500);
  }
});

router.post('/refresh', async (req, res) => {
  try {
    const token = await storeUtils.get(req.body.refresh_token);
    if (token) {
      const decoded = tokenUtils.verifyJWTExpired(token);
      const jwt = tokenUtils.createJWT({ email: decoded.email });
      return res.status(200).send({ jwt });
    }
    return handleError(res, new Error('Unauthorised'), 403);
  } catch (e) {
    logger(e);
    return handleError(res, new Error('Unexpected Error'), 500);
  }
});

module.exports = router;
