const express = require('express');
const User = require('../models/userModel');
const handleError = require('../handleError');
const tokenUtils = require('../utils/tokenUtils');
const store = require('../utils/storeUtils');
const logger = require('../logger');

const router = express.Router();

router.get('/', (req, res) => res.status(201).send({
  foo: 'bar',
}));

router.post('/', async (req, res) => {
  try {
    const savedUser = await User.build(req.body).save();
    const jwt = tokenUtils.createJWT({ email: savedUser.dataValues.email });

    // eslint-disable-next-line camelcase
    const refresh_token = await tokenUtils.generateRefreshTokenPromise();
    try {
      await store.set(refresh_token, jwt);
    } catch (e) {
      return handleError(res, new Error('Unexpected Error'), 500);
    }

    return res.status(201).send({
      jwt,
      refresh_token,
    });
  } catch (e) {
    logger(e);
    return handleError(res, e);
  }
});
module.exports = router;
