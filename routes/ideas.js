const express = require('express');
const handleError = require('../handleError');
const Idea = require('../models/ideaModel');
const logger = require('../logger');

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const savedIdeaInstance = await Idea.build(req.body).save();
    const savedIdea = savedIdeaInstance.dataValues;
    logger(savedIdea);
    return res.status(201).send({
      id: savedIdea.id,
      content: savedIdea.content,
      impact: savedIdea.impact,
      ease: savedIdea.ease,
      confidence: savedIdea.confidence,
      average_score: savedIdea.average_score,
      created_at: new Date(savedIdea.created_at).getTime(),
    });
  } catch (e) {
    logger(e);
    return handleError(res, e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const ideaId = req.params.id;
    const result = await Idea.destroy({ where: { id: ideaId } });
    logger(`Deleting ${ideaId} result=${result}`);
    if (result) {
      return res.status(204).send();
    }
    throw new Error('Idea with given id not found');
  } catch (e) {
    logger(e);
    return handleError(res, e, 400);
  }
});

router.put('/:id', async (req, res) => {
  try {
    const ideaId = req.params.id;
    const retVal = await Idea.update(req.body, {
      where: { id: ideaId },
      returning: true,
      individualHooks: true,
    });
    logger('retval', retVal);
    const idea = retVal[1][0].dataValues;
    idea.created_at = new Date(idea.created_at).getTime();
    idea.updated_at = new Date(idea.updated_at).getTime();
    logger(`put id - ${JSON.stringify(idea)}`);

    return res.status(200).send(idea);
  } catch (e) {
    logger(e);
    return handleError(res, e, 400);
  }
});

router.get('/', async (req, res) => {
  try {
    const LIMIT = 10;
    let page = parseInt(req.query.page, 10);

    // eslint-disable-next-line no-restricted-globals
    if (isNaN(page)) {
      // default value
      page = 1;
    }

    const ideas = await Idea.findAll({ limit: LIMIT, offset: LIMIT * (page - 1) });
    const output = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < ideas.length; i++) {
      const item = ideas[i].dataValues;
      const obj = {
        id: item.id,
        content: item.content,
        impact: item.impact,
        ease: item.ease,
        confidence: item.confidence,
        created_at: new Date(item.created_at).getTime(),
        updated_at: new Date(item.updated_at).getTime(),
        average_score: item.average_score,
      };
      output.push(obj);
    }

    logger(JSON.stringify(output));
    return res.status(200).send(output);
  } catch (e) {
    logger(e);
    return handleError(res, e);
  }
});
module.exports = router;
