const express = require('express');
const handleError = require('../handleError');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    return res.status(200).send({
      email: req.user.email,
      name: req.user.name,
      avatar_url: req.user.avatar_url,
    });
  } catch (e) {
    return handleError(res, e);
  }
});
module.exports = router;
