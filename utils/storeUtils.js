// should use redis or memcached or other similar in a real app
const Store = require('redis');
const logger = require('../logger');

const store = Store.createClient(process.env.REDIS_URL);

store.on('ready', () => {
  logger('REDIS READY!');
});

store.on('error', (err) => {
  logger(`REDIS ERROR: ${err}`);
});

const set = (key, val) => new Promise((resolve, reject) => {
  store.set(key, val, (err, reply) => {
    if (err) {
      return reject(err);
    }

    return resolve(reply);
  });
});

const get = key => new Promise((resolve, reject) => {
  store.get(key, (err, reply) => {
    if (err) {
      return reject(err);
    }
    return resolve(reply);
  });
});

const remove = key => new Promise((resolve, reject) => {
  store.del(key, (err, reply) => {
    if (err) {
      return reject(err);
    }

    return resolve(reply);
  });
});

module.exports = {
  set,
  get,
  remove,
};
