const jwt = require('jsonwebtoken');
const UidGen = require('uid-generator');

const key = process.env.SECRET;
const expirationSec = parseInt(process.env.EXPIRATION_SEC, 10);
const createJWT = obj => jwt.sign(obj, key, { expiresIn: expirationSec });

const verifyJWT = token => jwt.verify(token, key);

const verifyJWTExpired = token => jwt.verify(token, key, { ignoreExpiration: true });
const generateRefreshTokenPromise = () => new UidGen(584).generate();

module.exports = {
  createJWT,
  generateRefreshTokenPromise,
  verifyJWT,
  verifyJWTExpired,
};
