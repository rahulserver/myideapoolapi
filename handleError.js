const handler = (res, error, status = 400) => res.status(status).send({
  message: error.message,
});

module.exports = handler;
