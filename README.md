# Local Setup

For local setup, simply create a .env file(gitignored) in project root which looks like this:

```
  DATABASE_URL=postgres://gwaqjxncivjwqp:234f4fe6916c24f3c415e78ddrrerrea01d7d142a50a480ef431654d@ec2-54-235-92-214.compute-1.amazonaws.com:5412/d4qi1aqf7oddr64
  DEBUG=http
  ROUNDS=10
  SECRET=somesecretnotforprod
  EXPIRATION_SEC=5
  ENVIRONMENT=dev
  PROD_URL=https://someapiurl.com
  REDIS_URL=redis://h:aaabber3d8c7e0e8fdf5bc0a81d3afa7bba38e5bc9fd1caa9ffccddrf98c0@ec2-34-231-52-24.compute-1.amazonaws.com:8901
```

(Values are not real above. Just use your own values)

# Running tests

Use the command `npm test` to run the tests on local server.
For testing the remote api server using same test suite, just replace the `ENVIROMENT=dev` in the .env file with `ENVIRONMENT=testProd` and then run `npm test`

# Notes

- Project uses redis for storing refresh tokens.
- Make sure to set all the variables as process.env vars in production environment. Just put the `ENVIRONMENT=prod`.
- App is hosted on heroku, and it idles after 15min. of inactivity. So please kindly hit the https://myideapoolapi.herokuapp.com/ in browser so that it awakes.
